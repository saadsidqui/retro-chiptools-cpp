#include <stdint.h>
#include <Arduino.h>
#include "chip/interface/IMemory.hpp"
#include "chip/CPU/W65C02.hpp"

const uint8_t ADDR_PINS[] = {37, 39, 41, 43, 45, 47, 49, 51, 53, A15, A14, A13, A10, A11, A12, 52};
const uint8_t DATA_PINS[] = {36, 38, 40, 42, 44, 46, 48, 50};

Chiptools::CPU::W65C02 *device = nullptr;

/*
int incomingByte = 0;
bool isAutoStep = false;
uint8_t cycleDelay = 16;
uint32_t currentCycle = 0;
*/

void setup() {
	Serial.begin(57600);

	pinMode(2, INPUT);

	device = new Chiptools::CPU::W65C02(
		ADDR_PINS, DATA_PINS, 30,
		29, 31, 32, 33,
		27, 28, 24,
		25, 22, 34,
		26, 35, 23
	);
	device->setup();

	device->test();
	delete device;

	/*
	Serial.println("'r' to reset, 'c' run 15 cycles, 'a' to auto-step (any key to stop)");
	Serial.println("Any other key to single step.");
	*/
}

void loop() {
	//isInSimulation = digitalRead(2);
	// if (Serial.available() > 0) {
	// 	incomingByte = Serial.read();
	// 	if (isAutoStep) {
	// 		isAutoStep = false;
	// 	} else if (incomingByte == 'a') {
	// 		isAutoStep = true;
	// 	} else if (incomingByte == 'r') {
	// 		Serial.println("Reset");
	// 		device->reset();
	// 		currentCycle = 0;
	// 	} else if (incomingByte == 'c') {
	// 		for(int i = 0; i < 15; ++i) {
	// 			currentCycle++;
	// 			Serial.print("Cycle ");
	// 			Serial.println(currentCycle);
	// 			device->singleStep();
	// 			/*
	// 			if (isInSimulation) {
	// 				device->singleStep();
	// 				device->singleStep();
	// 				device->singleStep();
	// 			}
	// 			*/
	// 			delay(cycleDelay);
	// 		}
	// 	} else {
	// 		currentCycle++;
	// 		Serial.print("Single step ");
	// 		Serial.println(currentCycle);
	// 		device->singleStep();
	// 		/*
	// 		if (isInSimulation) {
	// 			device->singleStep();
	// 			device->singleStep();
	// 			device->singleStep();
	// 		}
	// 		*/
	// 	}
	// 	Serial.flush();
	// } else if (isAutoStep) {
	// 	currentCycle++;
	// 	Serial.print("Auto-cycle ");
	// 	Serial.println(currentCycle);
	// 	device->singleStep();
	// 	/*
	// 	if (isInSimulation) {
	// 		device->singleStep();
	// 		device->singleStep();
	// 		device->singleStep();
	// 	}
	// 	*/
	// 	delay(cycleDelay);
	// }
}