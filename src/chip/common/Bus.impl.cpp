#include "chip/common/Bus.cpp"

namespace Chiptools {
	template class Bus<uint16_t>;
	template class Bus<uint8_t>;
} // namespace Chiptools {