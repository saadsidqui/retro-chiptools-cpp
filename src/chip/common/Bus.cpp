#include <stdlib.h>
#include <Arduino.h>
#include "chip/common/Bus.hpp"

namespace Chiptools {
	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	Bus<T>::Bus(const uint8_t pins[], const uint8_t pinCount) noexcept:
		m_pinCount(pinCount) {
		m_pins = (uint8_t *) malloc(m_pinCount * sizeof(uint8_t));
		for (uint8_t i = 0; i < m_pinCount; ++i)
			m_pins[i] = pins[i];
	}

	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	Bus<T>::~Bus() noexcept {
		free(m_pins);
	}

	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	void Bus<T>::getPins(uint8_t pins[], const uint8_t howMany) const noexcept {
		uint8_t limit = min(m_pinCount, howMany);
		for (uint8_t i = 0; i < limit; ++i)
			pins[i] = m_pins[i];
	}

	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	void Bus<T>::setMode(const uint8_t mode) const noexcept {
		for (uint8_t i = 0; i < m_pinCount; ++i)
			pinMode(m_pins[i], mode);
	}

	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	T Bus<T>::read() const noexcept {
		T result = 0;
		int32_t i = m_pinCount;
		while (i--)
			result = (result << 1) + digitalRead(m_pins[i]);
		return result;
	}

	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	void Bus<T>::write(T data) const noexcept {
		for (uint16_t i = 0; i < m_pinCount; ++i) {
			digitalWrite(m_pins[i], data & 0x1);
			data = data >> 1;
		}
	}
}	 // namespace Chiptools