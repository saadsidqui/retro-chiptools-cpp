#include <Arduino.h>
#include "chip/interface/IMemory.hpp"

namespace Chiptools::Memory {
	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	IMemory<TAddr, TWord>::IMemory(uint8_t wordSize, TAddr wordCount, uint32_t dataRetentionDelay) noexcept:
		m_wordSize(wordSize), m_wordCount(wordCount), m_dataMax((1 << wordSize) - 1), m_dataRetentionDelay(dataRetentionDelay) {
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	IMemory<TAddr, TWord>::~IMemory() noexcept {
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	bool IMemory<TAddr, TWord>::mscan() const noexcept {
		bool result = false;
		return result;
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	bool IMemory<TAddr, TWord>::march() const noexcept {
		bool result = false;
		return result;
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	bool IMemory<TAddr, TWord>::checkerboard() const noexcept {
		bool result = false;
		return result;
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	void IMemory<TAddr, TWord>::dump(Chiptools::Memory::IMemory<TAddr, TWord> *device, TAddr limit) const noexcept {
		const uint8_t LINE_WIDTH = 16;
		uint8_t		  data[LINE_WIDTH];
		char		  buf[7 + ((LINE_WIDTH * 3) - 1)];
		limit = min(limit, device->getWordCount());
		for (uint32_t i = 0; i < limit; i += LINE_WIDTH) {
			for (uint8_t j = 0; j < LINE_WIDTH; ++j)
				data[j] = device->read(i + j);

			sprintf(
				buf,
				"%04lX:  %02X %02X %02X %02X %02X %02X %02X %02X | %02X %02X %02X %02X %02X %02X %02X %02X",
				i, data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]);
			Serial.println(buf);
			Serial.flush();
		}
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	bool IMemory<TAddr, TWord>::test() noexcept {
		bool result = false;
		return result;
	}
}	 // namespace Chiptools