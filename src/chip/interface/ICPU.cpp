#include "chip/interface/ICPU.hpp"

namespace Chiptools::CPU {
	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	ICPU<TAddr, TWord>::ICPU(uint8_t wordSize, uint8_t addrSize) noexcept:
		m_wordSize(wordSize), m_addrSize(addrSize), m_addrMax((1 << addrSize) - 1), m_dataMax((1 << wordSize) - 1) {
	}

	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	ICPU<TAddr, TWord>::~ICPU() noexcept {
	}
}	 // namespace Chiptools