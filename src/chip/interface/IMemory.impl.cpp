#include "chip/interface/IMemory.cpp"

namespace Chiptools::Memory {
	template class IMemory<uint16_t, uint8_t>;
} // namespace Chiptools {
