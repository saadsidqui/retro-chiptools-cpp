#include <stdlib.h>
#include <Arduino.h>
#include "chip/cpu/W65C02.hpp"

namespace Chiptools::CPU {
	W65C02::W65C02(
		const uint8_t addrPins[], const  uint8_t dataPins[], const uint8_t BE,
		const uint8_t IRQ, const uint8_t ML, const uint8_t NC, const uint8_t NMI,
		const uint8_t PHI1O, const uint8_t Clock, const uint8_t PHI2O,
		const uint8_t RDY, const uint8_t RES, const uint8_t RW,
		const uint8_t SO, const uint8_t SYNC, const uint8_t VP
	) noexcept: ICPU(8, 16), m_pinBE(BE), m_pinIRQ(IRQ), m_pinML(ML),
		m_pinNMI(NMI), m_pinPHI1O(PHI1O), m_pinClock(Clock),
		m_pinPHI2O(PHI2O), m_pinRDY(RDY), m_pinRES(RES), m_pinRW(RW),
		m_pinSO(SO), m_pinSYNC(SYNC), m_pinVP(VP)
	{
		m_address = new Bus<uint16_t>(addrPins, m_addrSize);
		m_data	  = new Bus<uint8_t>(dataPins, m_wordSize);
	}

	W65C02::~W65C02() noexcept {
		delete m_address;
		delete m_data;
	}

	void W65C02::setup() noexcept {
		pinMode(m_pinBE, OUTPUT);
		digitalWrite(m_pinBE, LOW);

		pinMode(m_pinIRQ, OUTPUT);
		digitalWrite(m_pinIRQ, HIGH);

		pinMode(m_pinML, INPUT);

		pinMode(m_pinNC, INPUT); 	// No-Connect pin. Just in case it's actually connected to the Arduino
									// we set this to input so that it's in a high-Z state.

		pinMode(m_pinNMI, OUTPUT);
		digitalWrite(m_pinNMI, HIGH);

		pinMode(m_pinPHI1O, INPUT);

		pinMode(m_pinClock, OUTPUT);
		digitalWrite(m_pinClock, HIGH);

		pinMode(m_pinPHI2O, INPUT);

		pinMode(m_pinRDY, INPUT_PULLUP);

		pinMode(m_pinRES, OUTPUT);
		digitalWrite(m_pinRES, HIGH);

		pinMode(m_pinRW, INPUT);

		pinMode(m_pinSO, OUTPUT);
		digitalWrite(m_pinSO, HIGH);

		pinMode(m_pinSYNC, INPUT);

		pinMode(m_pinVP, INPUT);

		m_address->setMode(INPUT);

		m_data->setMode(INPUT);
		this->reset();
	};

	void W65C02::reset() noexcept {
		digitalWrite(m_pinRES, LOW);
		digitalWrite(m_pinClock, HIGH);
		delayMicroseconds(m_phaseDuration);

		digitalWrite(m_pinClock, LOW);
		delay(m_phaseDuration);

		digitalWrite(m_pinRES, HIGH);
		delay(m_phaseDuration);

		digitalWrite(m_pinClock, HIGH);
		m_currentCycle = 0;
		this->printSummary();
	}

	bool W65C02::test() noexcept {
		int32_t i = 0xFFFF;
		bool pass = false;
		uint8_t step = 0;
		uint16_t expectedAddr = 0xFFFC;
		unsigned long s, e, diff;
		int8_t retries = 25;

		Serial.println("BEGIN Test");
		this->reset();

		while ((step < 2) && (retries--)) {
			this->singleStep();
			if (m_address->read() == expectedAddr) {
				step++;
				if (step == 1) {
					expectedAddr = 0xFFFD;
				} else {
					expectedAddr = 0xEAEA;
					pass = true;
				}
			} else if (step > 0) {
				step = 0;
				expectedAddr = 0xFFFC;
			}
		}

		if (!pass) {
			Serial.println("Failed to detect RESET sequence");
			return false;
		}

		Serial.println("TEST: RESET sequence detected");

		uint8_t *addrPins = (uint8_t *) malloc(m_addrSize * sizeof(uint8_t));
		m_address->getPins(addrPins, m_addrSize);

		int32_t j;
		uint16_t readAddr;

		s = micros();
		while (i--) {
			digitalWrite(m_pinClock, LOW);
			delayMicroseconds(1);
			digitalWrite(m_pinClock, HIGH);
			delayMicroseconds(1);
			digitalWrite(m_pinClock, LOW);
			delayMicroseconds(1);
			digitalWrite(m_pinClock, HIGH);
			delayMicroseconds(1);

			j = m_addrSize;
			readAddr = 0;
			while (j--)
				readAddr = (readAddr << 1) + digitalRead(addrPins[j]);

			if (readAddr == expectedAddr) {
				pass = false;
				break;
			};
			expectedAddr++;
		}

		if (pass) {
			e = micros();
			diff = e - s;

			Serial.print("TEST: Passed in ");
			Serial.println(diff);
		} else {
			Serial.println("Error detected");
		}
		free(addrPins);
		return pass;
	}

	void W65C02::singleStep() noexcept {
		digitalWrite(m_pinClock, LOW);
		delayMicroseconds(m_phaseDuration);
		digitalWrite(m_pinClock, HIGH);
		delayMicroseconds(m_phaseDuration);
		m_currentCycle++;
		this->printSummary();
	}

	void W65C02::printSummary() const noexcept {
		char buff[256] = {0};
		uint8_t rw = digitalRead(m_pinRW);
		uint8_t rdy = digitalRead(m_pinRDY);
		sprintf(
			buff,
			"CYCLE:%lu \t RDY: %c %u \tADDR: %04X \t DATA: %02X \t RW: %c %u",
			m_currentCycle,
			rdy ? 'Y' : 'N',
			rdy,
			m_address->read(),
			m_data->read(),
			rw ? 'R' : 'W',
			rw
		);
		Serial.println(buff);
	}
}	 // namespace Chiptools