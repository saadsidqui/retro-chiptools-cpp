#include <stdlib.h>
#include <Arduino.h>
#include "chip/memory/HM62256.hpp"

namespace Chiptools::Memory {
	HM62256::HM62256(const uint8_t addrPins[], const  uint8_t dataPins[], const  uint8_t CSPin, const  uint8_t WEPin, const  uint8_t OEPin) noexcept:
		IMemory(8, 32768, 300), m_CSPin(CSPin), m_WEPin(WEPin), m_OEPin(OEPin) {
		m_address = new Bus<uint16_t>(addrPins, 15);
		m_data	  = new Bus<uint8_t>(dataPins, 8);
	}

	HM62256::~HM62256() noexcept {
		delete m_address;
		delete m_data;

	}

	void HM62256::setup() const noexcept {
		digitalWrite(m_CSPin, HIGH);
		pinMode(m_CSPin, OUTPUT);

		digitalWrite(m_WEPin, HIGH);
		pinMode(m_WEPin, OUTPUT);

		digitalWrite(m_OEPin, HIGH);
		pinMode(m_OEPin, OUTPUT);

		m_address->write(0);
		m_address->setMode(OUTPUT);
	};

	uint8_t HM62256::read(uint16_t addr) const noexcept {
		uint8_t result = 0;

		m_data->setMode(INPUT);
		m_address->write(addr);
		digitalWrite(m_CSPin, LOW);
		digitalWrite(m_OEPin, LOW);
		delayMicroseconds(1);
		result = m_data->read();
		digitalWrite(m_OEPin, HIGH);
		digitalWrite(m_CSPin, HIGH);
		delayMicroseconds(1);
		return result;
	}

	void HM62256::write(uint16_t addr, uint8_t data) const noexcept {
		digitalWrite(m_OEPin, HIGH);
		delayMicroseconds(1);

		m_address->write(addr);
		delayMicroseconds(1);

		m_data->setMode(OUTPUT);
		m_data->write(data);

		digitalWrite(m_CSPin, LOW);
		digitalWrite(m_WEPin, LOW);
		delayMicroseconds(1);

		digitalWrite(m_WEPin, HIGH);
		digitalWrite(m_CSPin, HIGH);
		delayMicroseconds(1);
	}
}	 // namespace Chiptools