#include <stdlib.h>
#include <Arduino.h>
#include "chip/memory/AT28C256.hpp"

namespace Chiptools::Memory {
	AT28C256::AT28C256(const uint8_t addrPins[], const uint8_t dataPins[], const uint8_t CSPin, const uint8_t WEPin, const uint8_t OEPin) noexcept:
		IMemory(8, 32768, 300), m_CSPin(CSPin), m_WEPin(WEPin), m_OEPin(OEPin) {
		m_address = new Bus<uint16_t>(addrPins, 15);
		m_data	  = new Bus<uint8_t>(dataPins, 8);
	}

	AT28C256::~AT28C256() noexcept {
		delete m_address;
		delete m_data;
	}

	void AT28C256::setup() const noexcept {
		digitalWrite(m_CSPin, HIGH);
		pinMode(m_CSPin, OUTPUT);

		digitalWrite(m_WEPin, HIGH);
		pinMode(m_WEPin, OUTPUT);

		digitalWrite(m_OEPin, HIGH);
		pinMode(m_OEPin, OUTPUT);

		m_address->write(0);
		m_address->setMode(OUTPUT);

		this->disableSDP();
		delay(10);
	};

	uint8_t AT28C256::read(const uint16_t addr) const noexcept {
		uint8_t result = 0;

		m_data->setMode(INPUT);
		m_address->write(addr);
		digitalWrite(m_CSPin, LOW);
		digitalWrite(m_OEPin, LOW);
		delayMicroseconds(1);
		result = m_data->read();
		digitalWrite(m_OEPin, HIGH);
		digitalWrite(m_CSPin, HIGH);
		delayMicroseconds(1);
		return result;
	}

	void AT28C256::write(const uint16_t addr, const uint8_t data) const noexcept {
		digitalWrite(m_OEPin, HIGH);
		delayMicroseconds(1);

		m_address->write(addr);
		m_data->setMode(OUTPUT);
		delayMicroseconds(1);

		digitalWrite(m_CSPin, LOW);
		digitalWrite(m_WEPin, LOW);
		delayMicroseconds(1);

		m_data->write(data);
		delayMicroseconds(1);

		digitalWrite(m_WEPin, HIGH);
		digitalWrite(m_CSPin, HIGH);
		delay(10);
	}

	void AT28C256::enableSDP() const noexcept {
	}

	void AT28C256::disableSDP() const noexcept {
		/** TODO: Fix this */
		/**
		 * This function will meet the 150us criteria for disabling SDP.
		 * The function was timer over 1000 iterations, here are the results:
		 * 			SEQ		STEP
		 * MIN:		764		127
		 * AVG:		804.28	133.73
		 * MAX:		808		134
		*/
		uint8_t *addrPins = (uint8_t *) malloc(15 * sizeof(uint8_t));
		uint8_t *dataPins = (uint8_t *) malloc(8 * sizeof(uint8_t));
		m_address->getPins(addrPins, 15);
		m_data->getPins(dataPins, 8);

		digitalWrite(m_OEPin, HIGH);
		delayMicroseconds(1);

		digitalWrite(m_CSPin, LOW);
		m_data->setMode(OUTPUT);
		delayMicroseconds(1);

		// ====== STEP 1 ======
		digitalWrite(23, 1);
		digitalWrite(24, 0);
		digitalWrite(25, 1);
		digitalWrite(30, 0);
		digitalWrite(34, 1);
		digitalWrite(28, 0);
		digitalWrite(26, 1);
		digitalWrite(27, 0);
		digitalWrite(29, 1);
		digitalWrite(31, 0);
		digitalWrite(33, 1);
		digitalWrite(35, 0);
		digitalWrite(37, 1);
		digitalWrite(39, 0);
		digitalWrite(41, 1);

		digitalWrite(38, 1);
		digitalWrite(40, 0);
		digitalWrite(42, 1);
		digitalWrite(44, 0);
		digitalWrite(46, 1);
		digitalWrite(47, 0);
		digitalWrite(45, 1);
		digitalWrite(43, 0);

		//delayMicroseconds(1);
		digitalWrite(m_WEPin, LOW);
		//delayMicroseconds(1);
		digitalWrite(m_WEPin, HIGH);
		// ====== END 1 ======

		// ====== STEP 2 ======
		digitalWrite(23, 0);
		digitalWrite(24, 1);
		digitalWrite(25, 0);
		digitalWrite(30, 1);
		digitalWrite(34, 0);
		digitalWrite(28, 1);
		digitalWrite(26, 0);
		digitalWrite(27, 1);
		digitalWrite(29, 0);
		digitalWrite(31, 1);
		digitalWrite(33, 0);
		digitalWrite(35, 1);
		digitalWrite(37, 0);
		digitalWrite(39, 1);
		digitalWrite(41, 0);

		digitalWrite(38, 0);
		digitalWrite(40, 1);
		digitalWrite(42, 0);
		digitalWrite(44, 1);
		digitalWrite(46, 0);
		digitalWrite(47, 1);
		digitalWrite(45, 0);
		digitalWrite(43, 1);

		//delayMicroseconds(1);
		digitalWrite(m_WEPin, LOW);
		//delayMicroseconds(1);
		digitalWrite(m_WEPin, HIGH);
		// ====== END 2 ======

		// ====== STEP 3 ======
		digitalWrite(23, 1);
		digitalWrite(24, 0);
		digitalWrite(25, 1);
		digitalWrite(30, 0);
		digitalWrite(34, 1);
		digitalWrite(28, 0);
		digitalWrite(26, 1);
		digitalWrite(27, 0);
		digitalWrite(29, 1);
		digitalWrite(31, 0);
		digitalWrite(33, 1);
		digitalWrite(35, 0);
		digitalWrite(37, 1);
		digitalWrite(39, 0);
		digitalWrite(41, 1);

		digitalWrite(38, 1);
		digitalWrite(40, 0);
		digitalWrite(42, 0);
		digitalWrite(44, 0);
		digitalWrite(46, 0);
		digitalWrite(47, 0);
		digitalWrite(45, 0);
		digitalWrite(43, 0);

		//delayMicroseconds(1);
		digitalWrite(m_WEPin, LOW);
		//delayMicroseconds(1);
		digitalWrite(m_WEPin, HIGH);
		// ====== END 3 ======

		// ====== STEP 4 ======
		digitalWrite(23, 1);
		digitalWrite(24, 0);
		digitalWrite(25, 1);
		digitalWrite(30, 0);
		digitalWrite(34, 1);
		digitalWrite(28, 0);
		digitalWrite(26, 1);
		digitalWrite(27, 0);
		digitalWrite(29, 1);
		digitalWrite(31, 0);
		digitalWrite(33, 1);
		digitalWrite(35, 0);
		digitalWrite(37, 1);
		digitalWrite(39, 0);
		digitalWrite(41, 1);

		digitalWrite(38, 1);
		digitalWrite(40, 0);
		digitalWrite(42, 1);
		digitalWrite(44, 0);
		digitalWrite(46, 1);
		digitalWrite(47, 0);
		digitalWrite(45, 1);
		digitalWrite(43, 0);

		//delayMicroseconds(1);
		digitalWrite(m_WEPin, LOW);
		//delayMicroseconds(1);
		digitalWrite(m_WEPin, HIGH);
		// ====== END 4 ======

		// ====== STEP 5 ======
		digitalWrite(23, 0);
		digitalWrite(24, 1);
		digitalWrite(25, 0);
		digitalWrite(30, 1);
		digitalWrite(34, 0);
		digitalWrite(28, 1);
		digitalWrite(26, 0);
		digitalWrite(27, 1);
		digitalWrite(29, 0);
		digitalWrite(31, 1);
		digitalWrite(33, 0);
		digitalWrite(35, 1);
		digitalWrite(37, 0);
		digitalWrite(39, 1);
		digitalWrite(41, 0);

		digitalWrite(38, 0);
		digitalWrite(40, 1);
		digitalWrite(42, 0);
		digitalWrite(44, 1);
		digitalWrite(46, 0);
		digitalWrite(47, 1);
		digitalWrite(45, 0);
		digitalWrite(43, 1);

		//delayMicroseconds(1);
		digitalWrite(m_WEPin, LOW);
		//delayMicroseconds(1);
		digitalWrite(m_WEPin, HIGH);
		// ====== END 5 ======

		// ====== STEP 6 ======
		digitalWrite(23, 1);
		digitalWrite(24, 0);
		digitalWrite(25, 1);
		digitalWrite(30, 0);
		digitalWrite(34, 1);
		digitalWrite(28, 0);
		digitalWrite(26, 1);
		digitalWrite(27, 0);
		digitalWrite(29, 1);
		digitalWrite(31, 0);
		digitalWrite(33, 1);
		digitalWrite(35, 0);
		digitalWrite(37, 1);
		digitalWrite(39, 0);
		digitalWrite(41, 1);

		digitalWrite(38, 0);
		digitalWrite(40, 0);
		digitalWrite(42, 1);
		digitalWrite(44, 0);
		digitalWrite(46, 0);
		digitalWrite(47, 0);
		digitalWrite(45, 0);
		digitalWrite(43, 0);

		//delayMicroseconds(1);
		digitalWrite(m_WEPin, LOW);
		//delayMicroseconds(1);
		digitalWrite(m_WEPin, HIGH);
		// ====== END 6 ======

		delayMicroseconds(1);
		digitalWrite(m_CSPin, HIGH);

		free(addrPins);
		free(dataPins);
	}
}	 // namespace Chiptools