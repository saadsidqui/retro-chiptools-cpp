#ifndef INCLUDE_CHIP_COMMON_BUS_HPP
#define INCLUDE_CHIP_COMMON_BUS_HPP

#define INCLUDE_CHIP_COMMON_BUS_TEMPLATE template <typename T>

#include <stdint.h>

namespace Chiptools {
	INCLUDE_CHIP_COMMON_BUS_TEMPLATE
	class Bus {
		private:
		uint8_t	 m_pinCount;
		uint8_t *m_pins;

		public:
		Bus(const uint8_t pins[], const uint8_t pinCount) noexcept;
		~Bus() noexcept;

		uint8_t getPinCount() const noexcept { return m_pinCount; };
		void getPins(uint8_t pins[], const uint8_t howMany) const noexcept;
		void setMode(const uint8_t mode) const noexcept;
		T	 read() const noexcept;
		void write(const T data) const noexcept;
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_COMMON_BUS_HPP */
