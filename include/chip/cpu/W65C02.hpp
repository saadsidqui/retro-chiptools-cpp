#ifndef INCLUDE_CHIP_CPU_W65C02_HPP
#define INCLUDE_CHIP_CPU_W65C02_HPP

#include <stdint.h>
#include "chip/interface/ICPU.hpp"
#include "chip/common/Bus.hpp"

namespace Chiptools::CPU {
	class W65C02: public ICPU<uint16_t, uint8_t> {
		private:
		const uint8_t PWL_CLOCK_LOW_MIN_NS = 250;
		const uint8_t PWH_CLOCK_HI_MIN_NS = 250;
		const uint8_t AH_ADDR_HOLD_MIN_NS = 10;
		const uint8_t ADS_ADDR_SETUP_MAX_NS = 150;
		const uint16_t ACC_ACCESS_MIN_NS = 290;

		Bus<uint16_t> *m_address;
		Bus<uint8_t> *m_data;
		uint8_t m_pinBE;
		uint8_t m_pinIRQ;
		uint8_t m_pinML;
		uint8_t m_pinNC;		// No-Connect pin. See setup() body for info.
		uint8_t m_pinNMI;
		uint8_t m_pinPHI1O;
		uint8_t m_pinClock;
		uint8_t m_pinPHI2O;
		uint8_t m_pinRDY;
		uint8_t m_pinRES;
		uint8_t m_pinRW;
		uint8_t m_pinSO;
		uint8_t m_pinSYNC;
		uint8_t m_pinVP;

		uint16_t m_phaseDuration = 1;
		uint32_t m_currentCycle = 0;

		public:
		W65C02(
			const uint8_t addrPins[], const  uint8_t dataPins[], const uint8_t BE,
			const uint8_t IRQ, const uint8_t ML, const uint8_t NC, const uint8_t NMI,
			const uint8_t PHI1O, const uint8_t Clock, const uint8_t PHI2O,
			const uint8_t RDY, const uint8_t RES, const uint8_t RW,
			const uint8_t SO, const uint8_t SYNC, const uint8_t VP
		) noexcept;
		~W65C02() noexcept;

		void setup() noexcept final override;
		void reset() noexcept final override;
		bool test() noexcept final override;

		void singleStep() noexcept;
		void printSummary() const noexcept;
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_CPU_W65C02_HPP */
