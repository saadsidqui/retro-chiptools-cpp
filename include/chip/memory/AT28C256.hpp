#ifndef INCLUDE_CHIP_MEMORY_AT28C256_HPP
#define INCLUDE_CHIP_MEMORY_AT28C256_HPP

#include <stdint.h>
#include "chip/interface/IMemory.hpp"
#include "chip/common/Bus.hpp"

namespace Chiptools::Memory {
	class AT28C256: public IMemory<uint16_t, uint8_t> {
		private:
		Bus<uint16_t> *m_address;
		Bus<uint8_t> *m_data;
		uint8_t m_CSPin;
		uint8_t m_WEPin;
		uint8_t m_OEPin;

		public:
		AT28C256(const uint8_t addrPins[], const  uint8_t dataPins[], const  uint8_t CSPin, const  uint8_t WEPin, const  uint8_t OEPin) noexcept;
		~AT28C256() noexcept;

		void setup() const noexcept final override;
		uint8_t read(const uint16_t addr) const noexcept final override;
		void write(const uint16_t addr, const uint8_t data) const noexcept final override;

		void enableSDP() const noexcept;
		void disableSDP() const noexcept;
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_MEMORY_AT28C256_HPP */
