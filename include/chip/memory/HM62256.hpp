#ifndef INCLUDE_CHIP_MEMORY_HM62256_HPP
#define INCLUDE_CHIP_MEMORY_HM62256_HPP

#include <stdint.h>
#include "chip/interface/IMemory.hpp"
#include "chip/common/Bus.hpp"

namespace Chiptools::Memory {
	class HM62256: public IMemory<uint16_t, uint8_t> {
		private:
		Bus<uint16_t> *m_address;
		Bus<uint8_t> *m_data;
		uint8_t m_CSPin;
		uint8_t m_WEPin;
		uint8_t m_OEPin;

		public:
		HM62256(const uint8_t addrPins[], const  uint8_t dataPins[], const  uint8_t CSPin, const  uint8_t WEPin, const  uint8_t OEPin) noexcept;
		~HM62256() noexcept;

		void setup() const noexcept final override;
		uint8_t read(uint16_t addr) const noexcept final override;
		void write(uint16_t addr, uint8_t data) const noexcept final override;
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_MEMORY_HM62256_HPP */
