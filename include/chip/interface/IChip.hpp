#ifndef INCLUDE_CHIP_INTERFACE_ICHIP_HPP
#define INCLUDE_CHIP_INTERFACE_ICHIP_HPP

namespace Chiptools {
	class IChip {
		protected:
		virtual ~IChip() {};

		public:
		virtual bool test() noexcept = 0;
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_INTERFACE_ICHIP_HPP */
