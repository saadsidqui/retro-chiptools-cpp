#ifndef INCLUDE_CHIP_INTERFACE_ICPU_HPP
#define INCLUDE_CHIP_INTERFACE_ICPU_HPP

#include <stdint.h>
#include "chip/interface/IChip.hpp"

#define INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE template <typename TAddr, typename TWord>

namespace Chiptools::CPU {
	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	class ICPU: public IChip {
		protected:
		uint8_t m_wordSize;
		uint8_t m_addrSize;
		TAddr m_addrMax;
		TWord m_dataMax;
		ICPU(uint8_t wordSize, uint8_t addrSize) noexcept;
		~ICPU() noexcept;

		public:
		virtual void setup() noexcept = 0;
		virtual void reset() noexcept = 0;


		uint8_t getWordSize() { return m_wordSize; };
		uint8_t getAddrSize() { return m_addrSize; };
		TAddr getAddrMax() { return m_addrMax; };
		TWord getDataMax() { return m_dataMax; };
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_INTERFACE_ICPU_HPP */
