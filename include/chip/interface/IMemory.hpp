#ifndef INCLUDE_CHIP_INTERFACE_IMEMORY_HPP
#define INCLUDE_CHIP_INTERFACE_IMEMORY_HPP

#include <stdlib.h>
#include <stdint.h>
#include "chip/interface/IChip.hpp"

#define INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE template <typename TAddr, typename TWord>

namespace Chiptools::Memory {
	INCLUDE_CHIP_INTERFACE_IMEMORY_TEMPLATE
	class IMemory: public IChip {
		protected:
		uint8_t m_wordSize;
		TAddr m_wordCount;
		TWord m_dataMax;
		uint32_t m_dataRetentionDelay;
		IMemory(uint8_t wordSize, TAddr wordCount, uint32_t dataRetentionDelay) noexcept;
		~IMemory() noexcept;

		public:
		virtual void setup() const noexcept = 0;
		virtual TWord read(const TAddr addr) const noexcept = 0;
		virtual void write(const TAddr addr, const TWord data) const noexcept = 0;

		uint8_t getWordSize() { return m_wordSize; };
		TAddr getWordCount() { return m_wordCount; };
		TWord getDataMax() { return m_dataMax; };
		bool mscan() const noexcept;
		bool march() const noexcept;
		bool checkerboard() const noexcept;

		void dump(Chiptools::Memory::IMemory<TAddr, TWord> *device, TAddr limit) const noexcept;

		bool test() noexcept final override;
	};
}	 // namespace Chiptools

#endif /* INCLUDE_CHIP_INTERFACE_IMEMORY_HPP */
